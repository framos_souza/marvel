package com.api.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.api.model.Character;
import com.api.model.CharacterResponse;
import com.api.model.Serie;
import com.api.model.SerieResponse;
import com.api.service.MarvelService;

@Service
public class MarvelServiceImpl implements MarvelService {
	
	private final String API_PUBLIC_KEY = "23c877060167579098cb6d9abecab5ff";
	private final String API_PRIVATE_KEY = "6047c81bfef554c442c410f4ba5ac2417e93bc0e";
	
	@Override
	public List<Character> getMarvelCharacters() {
						
		String url = "http://gateway.marvel.com/v1/public/characters";				
		UriComponents uriComponents = getUriComponent(url);
		
		RestTemplate restTemplate = new RestTemplate();
		CharacterResponse r = restTemplate.getForObject(uriComponents.toUri(),CharacterResponse.class);
		List<Character> listPersonagens = r.getData().getResults();
		
		return listPersonagens;
	}

	@Override
	public List<Serie> getMarvelSeriesByCharacterId(Integer id) {
		
		String url = "http://gateway.marvel.com/v1/public/characters/{characterId}/series";								
		UriComponents uriComponents = getUriComponent(url);
		
		RestTemplate restTemplate = new RestTemplate();
		SerieResponse r = restTemplate.getForObject(uriComponents.toUriString(), SerieResponse.class, id);
		
		List<Serie> listSeries = r.getData().getResults();
		return listSeries;
	}
	
	private UriComponents getUriComponent(String url){
		
		String ts = Long.toString(System.currentTimeMillis());
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("ts", ts);
		params.add("apikey", this.API_PUBLIC_KEY);
		params.add("hash", DigestUtils.md5DigestAsHex((ts + this.API_PRIVATE_KEY + this.API_PUBLIC_KEY).getBytes()));						
		UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).queryParams(params).build();
		
		return uriComponents;		
	}
}
