package com.api.service;

import java.util.List;

import com.api.model.Character;
import com.api.model.Serie;

public interface MarvelService {	    
	    List<Character> getMarvelCharacters();
	    List<Serie> getMarvelSeriesByCharacterId(Integer id);
}
