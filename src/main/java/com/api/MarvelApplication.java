package com.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@SpringBootApplication
public class MarvelApplication {
    
	public static void main(String[] args) {
		SpringApplication.run(MarvelApplication.class, args);		
	}
}
