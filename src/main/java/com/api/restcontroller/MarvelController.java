package com.api.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.model.Character;
import com.api.model.Serie;
import com.api.service.MarvelService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class MarvelController {

	@Autowired
	private MarvelService marvelService;

	@RequestMapping(value="marvel",method = RequestMethod.GET)
	String readPersonagensESeries() {
		
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		
		List<Character> listaPersonagensToJson = new ArrayList<Character>();
		
		List<Character> listPersonagens = marvelService.getMarvelCharacters();
		for (Character personagem : listPersonagens) {
			List<Serie> listSeries = marvelService.getMarvelSeriesByCharacterId(personagem.getId());
			personagem.setListaSeries(listSeries);
			listaPersonagensToJson.add(personagem);
		}
		
		String json = objGson.toJson(listaPersonagensToJson);
		
		return json;
	}
}
