package com.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CharacterResponse {
	
	private Data data;
	
	public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
	
	public class Data {
		private List<Character> results;

		public List<Character> getResults() {
			return results;
		}

		public void setResults(List<Character> results) {
			this.results = results;
		}
	}
	
}
