package com.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SerieResponse {
	
	private Data data;
	
	public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
	
	public class Data {
		private List<Serie> results;

		public List<Serie> getResults() {
			return results;
		}

		public void setResults(List<Serie> results) {
			this.results = results;
		}
	}
	
}
