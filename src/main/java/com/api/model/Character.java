package com.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Character {

	Integer id;

	String name;

	String description;
	
	List<Serie> listaSeries;

	public List<Serie> getListaSeries() {
		return listaSeries;
	}

	public void setListaSeries(List<Serie> listaSeries) {
		this.listaSeries = listaSeries;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Characters [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
