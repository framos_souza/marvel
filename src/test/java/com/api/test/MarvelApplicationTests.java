package com.api.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.api.restcontroller.MarvelController;

@RunWith(SpringRunner.class)
@WebMvcTest(MarvelController.class)
public class MarvelApplicationTests {
	
	@Autowired
    private MockMvc mvc;

    @Test
    public void listaPersonagensESeries() throws Exception {
        this.mvc.perform(get("/marvel").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/json;charset=UTF-8"));
        
    }
}
